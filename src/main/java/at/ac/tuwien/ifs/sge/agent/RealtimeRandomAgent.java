package at.ac.tuwien.ifs.sge.agent;

import at.ac.tuwien.ifs.sge.core.agent.AbstractRealTimeGameAgent;
import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.core.game.RealTimeGame;
import at.ac.tuwien.ifs.sge.core.util.Util;

import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;

/*
    Real Time Random Agent

    A generic random agent for any real time strategy game.

    Performs random actions at a random interval.
    The intervals boundaries should be adjusted to the specific game for reasonable results.
 */
public class RealtimeRandomAgent<G extends RealTimeGame<A, ?>, A> extends AbstractRealTimeGameAgent<G, A> {

    /*
        The agent is started as a new process that gets passed three arguments:
            1. the player id of the player it is going to represent
            2. the player name of the player it is going to represent
            3. the game class name of the game it should play. This is only relevant if the agent supports multiple games.

        Since the 'Main-Class' attribute of the JAR file defined in the build.gradle points to this class
        a main method is defined which gets called when the engine starts the agent. The agent is then instantiated
        and started through the start() method, which also starts the agent's communication with the engine server.
     */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        var playerId = getPlayerIdFromArgs(args);
        var playerName = getPlayerNameFromArgs(args);
        var gameClass = ( Class<? extends RealTimeGame<Object, Object>> ) getGameClassFromArgs(args);
        var agent = new RealtimeRandomAgent<>(gameClass, playerId, playerName, -2);
        agent.start();
    }

    private static final int START_DELAY_MS = 1000;
    private static final int INTERVAL_MIN_MS = 1000;
    private static final int INTERVAL_MAX_MS = 1500;

    Random random = new Random();
    private final Timer timer = new Timer();

    /*
        A constructor calling the super constructor is required when extending the abstract agent.

        Since this agent can play any real time game the game class has a generic type.
     */
    public RealtimeRandomAgent(Class<G> gameClass, int playerId, String playerName, int logLevel) {
        super(gameClass, playerId, playerName, logLevel);
    }

    /*
        When the abstract agent receives an update from the server it is applied to the agents game after which
        the method onGameUpdate(A action, ActionResult result) is called. The parameters represent the
        action that has been applied as well as the result it created. The updated game state can be found
        in the protected field 'game' of the abstract agent implementation.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The random agent does not react to game updates.
     */
    @Override
    protected void onGameUpdate(HashMap<A, ActionResult> actionsWithResult) {}

    /*
        In case of an action that was sent to the server and is not valid or no longer valid, the server responds
        with an InvalidActionEvent which contains the action that has been rejected. The abstract agent then
        calls the onActionRejected(A action) method.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The random agent does not react to rejected actions.
     */
    @Override
    protected void onActionRejected(A action) {}

    /*
        After every agent is setup, the engine server issues a StartGameEvent which indicates that it is now
        ready to receive and process actions from the agents.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The random agent starts a new TimerTask at an interval of 1 second and a
        delay of 1 second, which is sending random actions to the engine server.
     */
    @Override
    public void startPlaying() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                var actions = getGame().getPossibleActions(playerId);
                log.debug("Possible actions: " + actions.size());
                var nextAction = Util.selectRandom(actions, random);
                sendAction(nextAction, System.currentTimeMillis() + 10);
            }
        },START_DELAY_MS, INTERVAL_MIN_MS + random.nextInt(INTERVAL_MAX_MS - INTERVAL_MIN_MS));
    }

    /*
        After the game is over the agent receives a TearDownEvent event, which signals it to stop
        the computation and shut down gracefully.

        The random agent stops the timer terminating all scheduled tasks.
    */
    @Override
    public void shutdown() {
        timer.cancel();
    }
}
